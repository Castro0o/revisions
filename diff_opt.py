#!/usr/bin/env python
import difflib
# reconstruct be, with marks indicating the changes

a = open("tmp_mw.txt", "r")
a = a.read()
b = open("tmp_mw_b.txt", "r")
b = b.read()


# a="Hello dog"
# b="He8lo god, finally"
s = difflib.SequenceMatcher(None, a, b)
x=''

for tag, i1, i2, j1, j2 in s.get_opcodes():
    if tag is "insert":
        b_str=b[j1:j2]
        x = x + "<ins>"+b_str+"</ins>"
#        print 'insert','b[{i1}:{i2}]'.format(i1=i1,i2=i2), b_str

    elif tag is "replace":
        b_str=b[j1:j2]
        x = x + "<samp>"+b_str+"</samp>"
#        print 'replace','b[{i1}:{i2}]'.format(i1=i1,i2=i2), b_str
        
    elif tag is 'equal':
        b_str=b[j1:j2]
#        print 'equal','b[{i1}:{i2}]'.format(i1=i1,i2=i2), b_str
        x = x + b_str
        
    elif tag is 'delete':
        b_str=b[j1:j2]
        if len(b_str)>0: #to avoid empty deleted parts
            x = x + "<del>"+b_str+"</del>"
#            print 'delete','a[{i1}:{i2}]'.format(i1=i1,i2=i2), b_str

        
print x


# #create html elements, and content
revision_div = ET.SubElement(template_container, 'div', attrib={'class':'revision'})
# metadata_div = ET.SubElement(revision_div, 'div', attrib={'class':'metadata'})
# metadata_div.text= user

content_div =  ET.SubElement(revision_div, 'div', attrib={'class':'content'})
content_p = ET.SubElement(content_div, 'p', attrib={'class':'content'})
content_p.text = content 

# usertime_p = ET.SubElement(metadata_div, 'p', attrib={'class':'usertime'})
# usertime_p.text= user + " " + timestamp

# comment_p = ET.SubElement(metadata_div, 'p', attrib={'class':'comment'})
# comment_p.text= comment

# save html
doctype = "<!DOCTYPE HTML>"
html = doctype + ET.tostring(template_tree,  method='html', encoding='utf-8', ) 
edited = open('revisions.html', 'w') 
edited.write(html)
edited.close(
)
