#!/usr/bin/env python
import difflib
import xml.etree.ElementTree as ET
import html5lib, time
from mwclient import Site
from argparse import ArgumentParser

# mw API: revisions
p = ArgumentParser()
p.add_argument("--host", default="en.wikipedia.org", help="url from wiki, without 'http://www'. E.g. 'wikipedia.org' for Wikipedia")
p.add_argument("--path", default="/w/", help="Article path. Note: should end with /")
p.add_argument("--page", default="2015 Baltimore protests", help="Title of wiki page you want to query")
p.add_argument("--start", default="20150501", help="Start query date YYYMMDD ")
p.add_argument("--end", default="20150503", help="End query date YYYMMDD ")
args=p.parse_args()
args.start=args.start+'000000'
args.end=args.end+'000000'

print args
site = Site(host=args.host, path=args.path)
page = site.Pages[args.page]
page_revisions = list(page.revisions( dir='newer',
                                      start=args.start,
                                      end=args.end,
                                      prop='ids|timestamp|flags|comment|user|content')
)
page_revisions.reverse()








# open template
template_file = open("revisions-template.html", "r")
template_tree = html5lib.parse(template_file, namespaceHTMLElements=False)
template_container = template_tree.find(".//div[@id='container']")


prev_content=''
for n, revision in enumerate(page_revisions):
    print revision['revid']
    timestamp = time.strftime('%Y-%m-%d %H:%M:%SZ',revision['timestamp'])
    comment = revision['comment']
    user = revision['user']
    content = revision['*']
    #html elements: revision container
    revision_div = ET.SubElement(template_container, 'div', attrib={'class':'revision'})
    #html elements: revision metadata
    metadata_div = ET.SubElement(revision_div, 'div', attrib={'class':'metadata'})
    usertime_p = ET.SubElement(metadata_div, 'p', attrib={'class':'usertime'})
    usertime_p.text= user + " " + timestamp
    comment_p = ET.SubElement(metadata_div, 'p', attrib={'class':'comment'})
    comment_p.text= comment
    #htm elements:content

    if prev_content:
        template_container = template_tree.find(".//div[@id='container']")
        content_div =  ET.SubElement(revision_div, 'div', attrib={'class':'content'})
        seqmatch = difflib.SequenceMatcher(None, prev_content, content)
        for tag, i1, i2, j1, j2 in seqmatch.get_opcodes():
            if tag is "insert":
                content_str=content[j1:j2]
                insert = ET.SubElement(content_div, 'samp', {'class':'insert'})
                insert.text = content_str
            elif tag is "replace":
                content_str=content[j1:j2]
                replace = ET.SubElement(content_div, 'samp', {'class':'replace'})
                replace.text = content_str
            elif tag is 'equal':
                content_str=content[j1:j2]
                pre = ET.SubElement(content_div, 'pre')
                pre.text = content_str

            elif tag is 'delete':
                content_str=content[j1:j2]
                delete = ET.SubElement(content_div, 'del')
                delete.text = content_str
            
    prev_content = content

    content_div_str = ET.tostring(template_container)
    #print content_div_str, type(content_div_str)
    # save html
    doctype = "<!DOCTYPE HTML>"
    html = doctype + ET.tostring(template_tree,  method='html', encoding='utf-8', ) 
    name = ((args.page).encode('utf-8')).replace(' ','-')
    edited = open('{}_{}.html'.format(name, revision['revid']), 'w') 
    edited.write(html)
    edited.close()
    template_container.remove(revision_div)




# s = difflib.SequenceMatcher(None, a, b)



