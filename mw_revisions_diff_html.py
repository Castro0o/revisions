#!/usr/bin/env python
from mwclient import Site
from argparse import ArgumentParser
from nltk import sent_tokenize
import shlex, subprocess
import xml.etree.ElementTree as ET
import difflib, time, html5lib
from htmltreediff import diff

'''   
**INSERT and DELETE revisions** 
# Output html file: revisions.html
# Example: mw_revision_diff.py --host en.wikipedia.org --path /w/ --page "2015 Baltimore protests" --start 20150401 --end  20150519
'''

p = ArgumentParser()
p.add_argument("--host", default="en.wikipedia.org", help="url from wiki, without 'http://www'. E.g. 'wikipedia.org' for Wikipedia")
p.add_argument("--path", default="/w/", help="Article path. Note: should end with /")
p.add_argument("--page", default="2015 Baltimore protests", help="Title of wiki page you want to query")
p.add_argument("--start", default="20150501", help="Start query date YYYMMDD ")
p.add_argument("--end", default="20150502", help="End query date YYYMMDD ")
args=p.parse_args()
args.start=args.start+'000000'
args.end=args.end+'000000'

print args
site = Site(host=args.host, path=args.path)
page = site.Pages[args.page]

# open template
template_file = open("revisions-template.html", "r")
template_tree = html5lib.parse(template_file, namespaceHTMLElements=False)
template_container = template_tree.find(".//div[@id='container']")

# convert with pandoc:
def pandoc2html(mw_content):
    '''convert individual mw sections to html'''    
    mw_content = mw_content.encode('utf-8')
    tmp_mw_file_path = '/home/andre/Documents/WdKA/courses/wikis/prototypes/tmp_mw.txt'
    tmp_mw_file = open(tmp_mw_file_path, 'w')
    tmp_mw_file.write(mw_content)
    tmp_mw_file.close()    
    # args_echo =shlex.split( ('echo "{}"'.format(mw_content)) )
    args_pandoc = shlex.split( 'pandoc -f mediawiki -t plain {}'.format( tmp_mw_file_path) )
    # p1 = subprocess.Popen(args_echo, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(args_pandoc, stdout=subprocess.PIPE)
    plain = (p2.communicate())[0]
    plain = plain.decode("utf-8")
    #print 'plain', p2, plain[0:10]
    return plain



# ASK FOR REVISIONS
page_revisions = list(page.revisions( dir='newer',
                                      start=args.start,
                                      end=args.end,
                                      prop='ids|timestamp|flags|comment|user|content')
)
page_revisions.reverse()
prev_content=[]
print len(page_revisions)
# from each revision select: timestamp, comment, user, content
for n, revision in enumerate(page_revisions):
     timestamp = time.strftime('%Y-%m-%d %H:%M:%SZ',revision['timestamp'])
     comment = revision['comment']
     user = revision['user']
     content = revision['*']
     # convertion to plain text
#     content = pandoc2html(content)
#     content = sent_tokenize(content)#
     content = content.splitlines()
     content=[ i.encode('utf-8') for i in content ]
     if n > 0: #avoid the first entry to be compared to nothing
         print n
         differ = difflib.Differ()     
         compare = differ.compare(content, prev_content)
         #create html elements, ,metadata and content
         revision_div = ET.SubElement(template_container, 'div', attrib={'class':'revision'})                
         metadata_div = ET.SubElement(revision_div, 'div', attrib={'class':'metadata'})
         metadata_div.text= user
         usertime_p = ET.SubElement(metadata_div, 'p', attrib={'class':'usertime'})
         usertime_p.text= user + " " + timestamp

         #  insert & delete content lines        
         content_div =  ET.SubElement(revision_div, 'div', attrib={'class':'content'})
         for diff in compare:
             if diff[0] is '+':                 
                 # find position of diff

                 content_p = ET.SubElement(content_div, 'p', attrib={'class':'content'})
                 content_p.text = diff[2:].decode('utf-8')

             elif diff[0] is '-':

                 # find position of diff
                 
                 strike = ET.SubElement(content_div, 's', attrib={'class':'content'})
                 content_p = ET.SubElement(strike, 'p', attrib={'class':'content'})
                 content_p.text = diff[2:].decode('utf-8')
     prev_content=content # previous content for compare 


# save html
doctype = "<!DOCTYPE HTML>"
html = doctype + ET.tostring(template_tree,  method='html', encoding='utf-8', ) 
edited = open('revisions.html', 'w') 
edited.write(html)
edited.close()


# TODO: Include ONLY differences btwn revisions? 

